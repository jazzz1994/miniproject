$(document).ready(function(){
    $(".btn").click(()=>{
        
        $(".invalid-feedback").hide();

        var fullname = $("#fullname").val();
        var phn      = $("#phonenumber").val();
        var email    = $("#emailaddress").val();
        var pattern  = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var details  = $("#enquiryinformation").val();
        

        var flag=0;


        
        //fullname validation starts
        if(fullname === "" || /[0-9]/.test(fullname))
        {
            $(".ffeed").html("Enter Valid name");
            $(".ffeed").show();
            flag++;
        }

        if(fullname !="" && fullname.length<3)
        {
            $(".ffeed").html("Minimum 3 Characters are Required");
            $(".ffeed").show();
            flag++;
        }

        if(fullname.length > 30)
        {
            $(".ffeed").html("Name Should Not Contain More Than 30 Characters");
            $(".ffeed").show();
            flag++;
        }
        //fullname validation ends



        //phone number validation starts
        if(phn === "")
        {
            $(".phnfeed").html("This field is required");
            $(".phnfeed").show();
            flag++;
        }

        if(phn.length !=="" && phn.length!=10)
        {
            $(".phnfeed").html("phone no should be 10 character long");
            $(".phnfeed").show();
            flag++;
        }

        if(/[^0-9]/.test(phn)){
            $(".phnfeed").html("phone no Should contain number");
            $(".phnfeed").show();
            flag++;
        }
        //phone number validation ends



        //email validation starts
        if(email==="")
        {
            $(".efeed").html("This field is required");
            $(".efeed").show();
            flag++;
        }

        
        if(!pattern.test(email.toLowerCase())) {
            console.log(!pattern.test(email.toLowerCase()));
            $(".efeed").html("Invalid Email");
            $(".efeed").show();
            flag++
        }
        //email validation ends


        
        



        //details validation starts
        if(details === "")
        {
            $(".dfeed").html("This field is required");
            $(".dfeed").show();
            flag++;
        }

        if(details !="" && details.length<3)
        {
            $(".dfeed").html("Minimum 3 Characters are Required");
            $(".dfeed").show();
            flag++;
        }

        if(details.length > 300)
        {
            $(".dfeed").html("Do Not Use More Than 300 Characters");
            $(".dfeed").show();
            flag++;
        }
         //details validation ends

         if(flag==0)
        {
            alert("Enquiry save successfully")
            window.location.href="./index.html";
        }
    });
});