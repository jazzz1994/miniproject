$(document).ready(function(){
        $(".btn").click(function(){
        var email = $("#email").val();
        var pass = $("#pass").val();
        var confPass = $("#cpass").val();
        var pattern  = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var prattern =/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
        var flag=0; 
        
        $(".invalid-feedback").hide();

        if (email === "") {
            $(".efeed").html("This field is required");
            $(".efeed").show();
            flag++;
        }

        if(email != "" && !pattern.test(email.toLowerCase())) {
            console.log(!pattern.test(email.toLowerCase()));
            $(".efeed").html("Invalid Email");
            $(".efeed").show();
            flag++
        }

        //password validation starts
        if(pass===""){
            $(".pfeed").html("This field is required");
            $(".pfeed").show();
            flag++;
        }

        if(pass !=="" && !prattern.test(pass)) {
            $(".pfeed").html("Invalid Password");
            $(".pfeed").show();
            flag++
        }
        
        
        if(pass !=="" && pass.length>8) {
            $(".pfeed").html("password should be less than 8 character");
            $(".pfeed").show();
            flag++
        }

        if (confPass === "") {
            $(".cpfeed").html("This field is required");
            $(".cpfeed").show();
            flag++;
        }

        if (confPass != pass) {
            $(".cpfeed").html("Password should be match");
            $(".cpfeed").show();
            flag++;
        }
        
         if(confPass !=="" && confPass.length>8) {
            $(".pfeed").html("password should be less than 8 character");
            $(".pfeed").show();
            flag++
        }

        if(flag==0)
        {
            alert("Password changed successfully")
            window.location.href="./index.html";
        }
    })
    
});