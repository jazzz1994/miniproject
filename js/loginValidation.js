$(".subbtn").click(()=>{
    


    $(".invalid-feedback").hide();
    var email = $("#email").val();
    var pass  = $("#pass").val();
    var epattern  =/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var prattern =/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    var flag  = 0;



     // email validations starts
     if (email === "") {
        $(".efeed").html("This field is required");
        $(".efeed").show();
        flag++;
    }

    if(email != "" && !epattern.test(email.toLowerCase())) {
        $(".efeed").html("Invalid Email");
        $(".efeed").show();
        flag++
    }

    // email validations ends

    if(pass===""){
        $(".pfeed").html("This field is required");
        $(".pfeed").show();
        flag++;
    }

    if(pass != "" && !prattern.test(pass)) {
        $(".pfeed").html("Invalid Password");
        $(".pfeed").show();
        flag++
    }
    
    if(pass !=="" && pass.length>8) {
        $(".pafeed").html("password should be less than 8 character");
        $(".pafeed").show();
        flag++
    }


    if(flag===0){
    
         var Students = JSON.parse(localStorage.getItem('students'));

         for(var i=0;i<Students.length;i++){
             if(email===Students[i].studentEmail && pass===Students[i].password){
                localStorage.setItem('credentials',JSON.stringify({email:email,name:Students[i].fullName}));
                window.location.href="./loginSuccess.html";
                 break;
             }
         }
        
}
});