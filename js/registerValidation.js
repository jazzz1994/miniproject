

if(localStorage.getItem('students')){

   var Students=JSON.parse(localStorage.getItem('students'));
}
else{
    var Students=[];
}


var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();

today = yyyy+ '-' + mm + '-' +dd ;

$("#dob").attr("max",today);


$(".btn").click(()=>{
    
    $(".invalid-feedback").hide();

    
    var sname  = $("#studname").val();
    var dob    = $("#dob").val();
    var pno    = $("#pno").val();
    var add    = $("#address").val();
    var email  = $("#email").val();
    var gender =$('input[name="gender"]:checked').val();
    var branch =$('#branch option:selected').val();
    var password=$('#password').val();
    var prattern =/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    var pattern= /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var flag  = 0;

    console.log(branch);
    
    
    
    // student name validation
    if(sname===""){
        $(".sfeed").html("This field is required");
        $(".sfeed").show();
        flag++;
    }

    if(sname!=="" && sname.length<3){
        
        $(".sfeed").html("Minimum Three Charaters are required");
        $(".sfeed").show();
        flag++;
    }

    if(sname.length>20){
        $(".sfeed").html("Name Should Not Contain More Than 60 Characters");
        $(".sfeed").show();
        flag++;
    }

    if(sname.length>20){
        $(".sfeed").html("Name Should Not Contain More Than 60 Characters");
        $(".sfeed").show();
        flag++;
    }

    if(/[0-9]/.test(sname)){
        $(".sfeed").html("Name should not contain number");
        $(".sfeed").show();
        flag++;
    }
   // student name validation end



    //dob validation
    if (dob === "") {
        $(".dfeed").html("This field is required");
        $(".dfeed").show();
        flag++;
    }
    //dob validation ends



    // phone number validation
    if(pno===""){
        $(".pfeed").html("This field is required");
        $(".pfeed").show();
        flag++;
    }

    
    if (pno !=="" && pno.length != 10) {
        $(".pfeed").html("phone no should be 10 character long");
        $(".pfeed").show();
        flag++;
    }

    if(/[^0-9]/.test(pno)){
        $(".pfeed").html("phone no should contain only number");
        $(".pfeed").show();
        flag++;
    }
      // phone number validation ends





    // address validation
    if (add === "") {
        $(".afeed").html("This field is required");
        $(".afeed").show();
        flag++;
    }


    if (add !== "" && add.length <20) {
        $(".afeed").html("address should be at least 20 character long");
        $(".afeed").show();
        flag++;
    }
    // address validation ends

    // branch validation
    if(branch==="Select Branch"){
        $(".brfeed").html("this field is required");
        $(".brfeed").show();
        flag++;
    }
    
    // branch validation ends
    
    // email validation
    if (email === "") {
        $(".efeed").html("This field is required");
        $(".efeed").show();
        flag++;
    }

    if(email !=="" && !pattern.test(email.toLowerCase())) {
        console.log(!pattern.test(email.toLowerCase()));
        $(".efeed").html("Invalid Email");
        $(".efeed").show();
        flag++
    }
    //email validation ends


    //password validation starts
    if(password===""){
        $(".pafeed").html("This field is required");
        $(".pafeed").show();
        flag++;
    }

    if(password !=="" && !prattern.test(password)) {
        $(".pafeed").html("Invalid Password");
        $(".pafeed").show();
        flag++
    }
    
    
    if(password !=="" && password.length>8) {
        $(".pafeed").html("password should be less than 8 character");
        $(".pafeed").show();
        flag++
    }
    //password  validation ends

if(flag===0){

    var redirect=true;

    for(var i=0;i<Students.length;i++){

        if(Students[i].studentEmail===email){
            $(".efeed").html("Email already Exists");
            $(".efeed").show();
            redirect=false;
            break;
        }
    }

    if(redirect){
        Students.push({
            fullName:sname,
            dateOfBirth:dob,
            gender:gender,
            phoneNo:pno,
            address:add,
            studentEmail:email,
            password:password,
            branch:branch
        });

        localStorage.setItem('students' ,JSON.stringify(Students));
        alert("You have successfully registered ");
        window.location.href="./index.html";
  }
}


});
